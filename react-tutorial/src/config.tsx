import RenderProps from "./components/menuItems/menuItems";
import RootComponent from "./components/root";
import UserInfo from "./components/concepts/hoc/userInfo";

export const Routes = [
  {
    component: RootComponent,
    compPath: "./components/root",
    path: "/",
  },
  {
    component: RenderProps,
    compPath: "./components/menuItems/menuItems",
    path: "/renderProps",
  },
  {
    component: UserInfo,
    compPath: "./components/concepts/hoc/userInfo",
    path: "/hoc",
  },
];
