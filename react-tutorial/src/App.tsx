import React, { Component, Suspense, lazy } from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
// import { Routes } from "./config";

const getComponent = (compPath: any) => {
  return lazy(() => import(compPath));
};
const RenderProps = lazy(() => import("./components/menuItems/menuItems"));
const RootComponent = lazy(() => import("./components/root"));
const UserInfo = lazy(() => import("./components/concepts/hoc/userInfo"));

class App extends Component {
  render() {
    return (
      <Router>
        <Suspense fallback={<div>Loading...</div>}>
          <Switch>
          <Route path="/" component={RootComponent} />
          <Route path="/hoc" component={UserInfo} />
            <Route exact path="/renderProps" component={RenderProps} />
          </Switch>
        </Suspense>
      </Router>
    );
  }
}

/*
            {Routes.map((route: any, i: number) => {
              return (
                <Route
                  exact
                  path={route.path}
                  key={i}
                  component={getComponent(route.compPath)}
                />
              );
            })}
            */

export default App;
