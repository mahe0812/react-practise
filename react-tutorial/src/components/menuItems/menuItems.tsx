import React from "react";
import styled from "styled-components";
import OutsideClickAlerter from "../concepts/renderProps/renderPropsContainer";

const DivWrapper = styled.div`
  width: 500px;
  height: 100px;
  background-color: grey;
`;
class MenuItem extends React.Component<{}, {}> {
    render() {
        return (
            <OutsideClickAlerter
            render={(isClickedOuside: boolean) => (
              <DivWrapper>
                <div>Click outside to get alerter: {`${isClickedOuside}`}</div>
              </DivWrapper>
            )}
          />
        )
    }
}

export default MenuItem;


