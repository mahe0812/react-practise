import React from "react";
import { Link } from "react-router-dom";

class RootComponent extends React.Component {
  render() {
    return (
      <div>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/renderProps">RenderProps</Link>
          </li>
          <li>
            <Link to="/hoc">HOC</Link>
          </li>
        </ul>
      </div>
    );
  }
}

export default RootComponent;
