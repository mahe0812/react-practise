import loadable from "react-loadable";

const loading = () => {
    return (<div>Loading...</div>)
}

const loadableWrapper = (component: any) => {
    loadable({
        loader: component,
        loading 
    })
}

export const conceptsCom = loadableWrapper(
    () => import("../components/concepts/renderProps/renderPropsContainer")
);