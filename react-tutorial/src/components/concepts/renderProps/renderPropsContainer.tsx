import * as React from "react";

interface IOutSideClickProps {
  render: (data: boolean) => JSX.Element;
}
interface IOutSideClickState {
  isClickedOutside: boolean;
}

/**
 * @description Component that alerts if you click outside of it
 *
 * Usage example :
 * <OutsideClickAlerter render={
 *     (clickedOuside: boolean) => <MyComponent clickedOuside={clickedOuside}/>
 *   }/>
 */
class RenderProps extends React.Component<
  IOutSideClickProps,
  IOutSideClickState
> {
  readonly state: IOutSideClickState = {
    isClickedOutside: false
  };
  private wrapperRef = React.createRef<HTMLDivElement>();
  componentDidMount() {
    document.addEventListener("mousedown", this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClickOutside);
  }

  /**
   * @description Alert to the component, if clicked on outside of element
   * @memberof OutsideClickAlerter
   */
  handleClickOutside = (event: MouseEvent) => {
    if (
      this.wrapperRef.current &&
      !this.wrapperRef.current.contains(event.target as Node)
    ) {
      this.setState({ isClickedOutside: true });
    } else {
      this.setState({ isClickedOutside: false });
    }
  };
  renderCodeBlock = () => {
    return `
      FACC -> isClickedOutside is recieved as props
      Children as function
        <div ref={this.wrapperRef}>
      {this.props.children(this.state.isClickedOutside)}
    </div>

    <OutsideClickAlerter>
          {(isClickedOuside: boolean) => (
              <OutSideClickWrapper>
                <div>Click outside to get alerter: {isClickedOuside}</div>
                )}
              </OutSideClickWrapper>


              Render Props - isClickedOutside is recieved as props to render
    <div ref={this.wrapperRef}>
        {this.props.render(this.state.isClickedOutside)}
      </div>

      <OutsideClickAlerter
            render={(isClickedOuside: boolean) => (
              <OutSideClickWrapper>
                <div>Click outside to get alerter: {isClickedOuside}</div>
              </OutSideClickWrapper>
            )}
       `;
  };
  render() {
    return (
      <div>
        <div ref={this.wrapperRef}>
          {this.props.render(this.state.isClickedOutside)}
        </div>
        {this.renderCodeBlock()}
      </div>
    );
  }
  /*
  <div ref={this.wrapperRef}>
        {this.props.children(this.state.isClickedOutside)}
      </div>
  */
}

export default RenderProps;
