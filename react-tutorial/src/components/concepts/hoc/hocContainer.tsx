import React from "react";

const HocContainer = (WrapperComponent: any) => {
    const userPermission = {access: "readonly"};
    return (props: any) => <WrapperComponent permission={userPermission} {...props}/>
};

export default HocContainer;