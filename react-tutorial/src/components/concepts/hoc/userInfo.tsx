import React from "react";
import HocContainer from "./hocContainer";

interface IUserInfo {
    permission: any;
}
class UserInfo extends React.Component<IUserInfo, {}>{
    render() {
        const {permission} = this.props;
        return (
            <div>This user has {permission.access} access</div>
        )
    }
}

export default HocContainer(UserInfo); 